======
yusers
======

Yunohost application for user management.

Configuration
=============

The application is configured through the Flask standard settings file, defined by ``YUSERS_SETTINGS`` environment variable.

These are Yusers specific options:

``YUSERS_LDAP_ADMIN_PASSWORD``
------------------------------
Password of the Yunohost administrator account.
Default is ``None``.

``YUSERS_LDAP_URI``
-------------------
URI of the LDAP server.
Default is ``ldap://localhost:389/``.

``YUSERS_PASSWORD_RESET_TIMEOUT``
---------------------------------
Number of days for which a password reset token is valid.
Default is ``3``.
