from setuptools import setup


def main():
    setup(author='Vlastimil Zíma')


if __name__ == '__main__':
    main()
