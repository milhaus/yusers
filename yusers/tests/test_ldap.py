from unittest import TestCase
from unittest.mock import call, patch, sentinel

from ldap import SCOPE_SUBTREE, LDAPError

from yusers import create_app
from yusers.ldap import ADMIN_DN, USERS_BASE, get_ldap_object, search, set_password


class GetLdapObjectTest(TestCase):
    def setUp(self):
        self.app = create_app(YUSERS_LDAP_URI=sentinel.uri, YUSERS_LDAP_ADMIN_PASSWORD=sentinel.password)

        patcher = patch('yusers.ldap.ReconnectLDAPObject', autospec=True)
        self.addCleanup(patcher.stop)
        self.ldap_mock = patcher.start()

    def test_connect(self):
        with self.app.test_request_context():
            result = get_ldap_object()

        calls = [call(sentinel.uri, retry_max=10, retry_delay=0.5),
                 call().simple_bind_s(ADMIN_DN, sentinel.password)]
        self.assertEqual(self.ldap_mock.mock_calls, calls)
        self.assertEqual(result, self.ldap_mock.return_value)

    def test_error(self):
        self.ldap_mock.return_value.simple_bind_s.side_effect = LDAPError

        with self.app.test_request_context():
            with self.assertRaises(LDAPError):
                get_ldap_object()

        calls = [call(sentinel.uri, retry_max=10, retry_delay=0.5),
                 call().simple_bind_s(ADMIN_DN, sentinel.password)]
        self.assertEqual(self.ldap_mock.mock_calls, calls)


class SearchTest(TestCase):
    def setUp(self):
        self.app = create_app(YUSERS_LDAP_URI=sentinel.uri, YUSERS_LDAP_ADMIN_PASSWORD=sentinel.password)

        patcher = patch('yusers.ldap.ReconnectLDAPObject', autospec=True)
        self.addCleanup(patcher.stop)
        self.ldap_mock = patcher.start()

    def test_search(self):
        self.ldap_mock.return_value.search_s.return_value = sentinel.result

        with self.app.test_request_context():
            result = search(sentinel.filter, sentinel.attrs)

        self.assertEqual(result, sentinel.result)
        calls = [call(sentinel.uri, retry_max=10, retry_delay=0.5),
                 call().simple_bind_s(ADMIN_DN, sentinel.password),
                 call().search_s(USERS_BASE, SCOPE_SUBTREE, sentinel.filter, sentinel.attrs)]
        self.assertEqual(self.ldap_mock.mock_calls, calls)


class SetPasswordTest(TestCase):
    def setUp(self):
        self.app = create_app(YUSERS_LDAP_URI=sentinel.uri, YUSERS_LDAP_ADMIN_PASSWORD=sentinel.password)

        patcher = patch('yusers.ldap.ReconnectLDAPObject', autospec=True)
        self.addCleanup(patcher.stop)
        self.ldap_mock = patcher.start()

    def test_set_password(self):
        with self.app.test_request_context():
            set_password(sentinel.uid, sentinel.password)

        calls = [call(sentinel.uri, retry_max=10, retry_delay=0.5),
                 call().simple_bind_s(ADMIN_DN, sentinel.password),
                 call().passwd_s(sentinel.uid, None, sentinel.password)]
        self.assertEqual(self.ldap_mock.mock_calls, calls)
