"""Yusers - Yunohost application for user management."""
import os

from flask import Flask
from flask_mail import Mail
from flask_wtf.csrf import CSRFProtect

__version__ = '0.0.1'


CSRF = CSRFProtect()
MAIL = Mail()


def create_app(**config):
    """Create and configure application."""
    app = Flask(__name__, instance_relative_config=True)
    app.config['YUSERS_LDAP_URI'] = os.environ.get('YUSERS_LDAP_URI', 'ldap://localhost:389/')
    app.config['YUSERS_LDAP_ADMIN_PASSWORD'] = os.environ.get('YUSERS_LDAP_ADMIN_PASSWORD')
    app.config['YUSERS_PASSWORD_RESET_TIMEOUT'] = os.environ.get('YUSERS_PASSWORD_RESET_TIMEOUT', 3)
    app.config.from_envvar('YUSERS_SETTINGS', silent=True)
    app.config.from_mapping(**config)

    # Set up CSRF protection.
    CSRF.init_app(app)
    # Set up mail
    MAIL.init_app(app)

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .password_reset import PASSWORD_RESET_BLUEPRINT
    app.register_blueprint(PASSWORD_RESET_BLUEPRINT)

    return app
