"""LDAP utilities."""
from typing import Dict, List, Tuple

from flask import current_app
from ldap import SCOPE_SUBTREE
from ldap.ldapobject import LDAPObject, ReconnectLDAPObject

ADMIN_DN = 'cn=admin,dc=yunohost,dc=org'
USERS_BASE = 'ou=users,dc=yunohost,dc=org'


def get_ldap_object() -> LDAPObject:
    """Return LDAP object."""
    ldap_obj = ReconnectLDAPObject(current_app.config['YUSERS_LDAP_URI'], retry_max=10, retry_delay=0.5)
    ldap_obj.simple_bind_s(ADMIN_DN, current_app.config['YUSERS_LDAP_ADMIN_PASSWORD'])
    return ldap_obj


def search(filter: str, attrs: List[str]) -> List[Tuple[str, Dict]]:
    """Search LDAP."""
    ldap_obj = get_ldap_object()
    return ldap_obj.search_s(USERS_BASE, SCOPE_SUBTREE, filter, attrs)


def set_password(uid: str, password: str) -> None:
    """Set user's password to LDAP."""
    ldap_obj = get_ldap_object()
    ldap_obj.passwd_s(uid, None, password)
